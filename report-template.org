#+AUTHOR: Yuji Kanagawa
#+EMAIL: yuji.kanagawa@oist.jp
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t   toc:nil \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:nil
#+OPTIONS:   author:t creator:nil timestamp:t email:t
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [11pt]
#+LATEX_HEADER: \usepackage{times}
#+LATEX_HEADER: \usepackage{amssymb}
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{amsfonts}
#+LATEX_HEADER: \usepackage{bm}
#+LATEX_HEADER: \usepackage{nicefrac}
#+LATEX_HEADER: \usepackage{xcolor}
#+LATEX_HEADER: \definecolor{twilightblue}{HTML}{363b74}
#+LATEX_HEADER: \definecolor{twilightpink}{HTML}{ef4f91}
#+LATEX_HEADER: \definecolor{twilightbluepurple}{HTML}{4d1b7b}
#+LATEX_HEADER: \definecolor{twilightpurple}{HTML}{673888}
#+LATEX_HEADER: \usepackage{hyperref}
#+LATEX_HEADER: \usepackage{cleveref}
#+LATEX_HEADER: \hypersetup{colorlinks=true, citecolor=twilightblue, linkcolor=twilightpink, linktocpage=true, urlcolor=twilightpurple}
#+LATEX_HEADER: \usepackage{geometry}
#+LATEX_HEADER: \geometry{left=25mm, right=25mm, top=30mm, bottom=30mm}
#+LATEX_HEADER: \newcounter{num}
#+LATEX_HEADER: \newcommand{\rnum}[1]{\setcounter{num}{#1}(\roman{num})}
#+LATEX_HEADER: \newcommand{\1}{\mathbb{I}}
#+LATEX_HEADER: \newcommand{\indic}[1]{\1_{#1}}
#+LATEX_HEADER: \usepackage{natbib}
#+LATEX_HEADER: \bibliographystyle{abbrvnat}
#+LATEX_HEADER: \setcitestyle{authoryear,open={(},close={)}}
#+LATEX_HEADER: \def\UrlBreaks{\do\/\do-}
