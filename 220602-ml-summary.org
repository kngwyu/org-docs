#+TITLE: Lecture summary: week1 and week2
#+SETUPFILE: report-template.org

This report is a summary of first two weeks lectures of [[https://emtiyaz.github.io/teaching/oist_B39_2022/main.html][Foundation of Machine Learning]].
My understanding could be biased, and I restructured the contents a bit.
Thus, readers are not encouraged to take this report as a general summary of the lecture.

* Day 1: Introduction

In the first day of the lecture, we discussed about what is machine learning (ML). We agree that ML is an approach that makes machines to learn something from data or experience. A short discussion about the taxonomy of ML followed. We discussed the differences between ML and related field such as artificial intelligence (AI), data mining, and statistics. There were considerable variety in understanding as expected, but to show my understanding:
+ AI is the broadest field. Many forms of 'interlligence' are studied.
+ Data mining is about extracting some useful knowledge or insights from (sometimes big) data, geared towards interpretation.
+ Statistics is more of mathematical properties of data, assuming random variables behind it.

Thus, ML's unique character would be 'learning from data'. I feel like sometimes ML is refereed as a prediction problem from data, but I think it's a bit broader.
We also went through the state of the art of ML. It has succeeded in many areas like recommendation system and facial recognition, there remain many problems such as biases and flexibility.

* Day 2: Overview of the ML procedure for decision making

We continued to go through the introduction and started to dive into ML methods. ML starts from acquiring data and defining a problem.
After getting some results from ML, we evaluate the result in some way, and use it to an actual decision making we need. All of data acquisition, problem definition, and model evaluation are as important as ML methods themselves. Simply, whether we can get some useful information depends on the problem definition, and we cannot even define the problem without data. 'Predicting something with whatever you can use' would not be a good ML problem. Evaluation is also important to get confident about our results. If the evaluation tells us that our model is bad, we have to not only tune our method and reconsider the whole process. The problem can be ill-posed and data can have unrelated features.

** Note on problem definition
Problem definition is especially important in the ML process. To build an intuition about reasonable problems, we did a series of exercises where we answer whether given problem statements are prediction or interpretation. The border can be sometimes vague, but generally, we call a problem 'prediction' when inputs and outputs are explicitly given. For example, predicting tomorrow's weather from past 10 days of weather is a prediction problem. However, followings are rather interpretation problems.
+ Problems that require complex logical structure. E.g., 'can drinking cause liver troubles'?
+ Prediction without any input specified. E.g., 'will the population of Japan increase'?

* Day3: Our first ML problem

We started to dive into the world of ML methods. First problem we encounter is regression. Regression is a family of supervised learning problems that:
+ Data consists of pairs $(y_n, \mathbf{x}_n)$, where $y_n$ is a scalar and $\mathbf{x}_n$ is a $D$ -dimensinal vector.
+ The goal is to find a function $f$ that approximate data well, i.e., $y_n \approx \mathbf{x}$.

The first model we use for regression is the linear regression model $f_\beta$, which does linear projection from $\mathbf{x}$ to $y$ by
$f(\mathbf{x}_n) = \beta_0 + \beta_1 x_n1 + ... + \beta x_{nD} = \tilde{x}^T_n \beta$.
This can be a rough approximation since it just draws line/plane/etc. in the data space, but it is very simple and thus a good starting point.

** $D > N$ Problem
Now $D$ is the dimensionality of input data ($\mathbf{x}$) and $N$ is the number of sample.
If $D$ is larger than $N$, we have an apparent problem: there can be infinitely many models that approximate the data well! However, this situation often happens with real data and it's (surprisingly) difficult to avoid it. For (an extreme) example, if we have 3 data points consisting of $(0.5, (1.0, 1.0)), (0.5, (1.0, 1.0)), (0.5, (1.0, 1.0))$, does it tell more than a single data point? The intrinsic dimension of data is unknown, and we need to choose a useful or reasonable answer from many choices.


* Day 4: Cost function
Now we have a problem and a model, so we then need a way to train our model to fit nicely with the data. For this purpose, we introduce a cost function that evaluate the fitness of a model to the data. Letting $\beta$ denote the parameter vector of our linear model, the cost function is written as $\mathcal{L}(\beta)$, where $\mathcal{L}$ is the capital of loss. Given the cost function, we can view regression as a minimization problem of $\mathcal{L}(\beta)$.

Aside from representing the fitness to the data, we hope that our cost function satisfy the following two properties. First, cost function should be symmetric around $0$. It should treat negative and positive errors with the same magnitude equally. Second, it should treat a huge error and a very huge error equally. If not, our method would be too sensitive to outliers. Surprisingly, the famous MSE loss $\mathcal{L}(\beta) = \frac{1}{2N} \sum_{n=0}^N (f_\beta(\mathbf{x}_n) - y_n) ^ 2$ does not satisfy the second property. We actually confirmed this fact by doing an exercise. MSE did too much for one outlier! In contrast, absolute loss $\mathcal{L}(\beta) = \frac{1}{N} \sum_{n=0}^N |f_\beta(\mathbf{x}_n) - y_n |$ behaved much robustly. In other words, MSE averages the data, while absolute loss finds an median value. However, MSE loss seems to used much frequently due to its good characteristic in terms of optimization (continuous, differentiable, and so on).
