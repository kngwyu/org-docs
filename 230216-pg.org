#+TITLE: A belief introduction to policy gradient
#+SETUPFILE: report-template.org
#+LATEX_HEADER: \usepackage[framemethod=tikz]{mdframed}

In this note, I briefly describe the policy gradient (PG) method in reinforcement learning (RL). PG is a family of RL methods that optimizes a parameterized policy using the gradient.

Let me introduce the Markov decision process (MDP) as follows:

\begin{mdframed}
\textbf{MDP}

MDP consists of
state space $\mathcal{S}$,
action space $\mathcal{A}$,
transitin probability $P: \mathcal{S} \times \mathcal{A} \rightarrow \mathcal{S}$,
reward function $r: \mathcal{S} \times \mathcal{A} \rightarrow \mathbb{R}$, and
discount factor $0 \leq \gamma < 1$.
\end{mdframed}

In MDP, an agent makes its decision based on a policy $\pi(a|s)$, which is a distribution of an action conditioned by a certain state. Then, the following quantities can be defined:

\begin{mdframed}
\textbf{Value Function}

State value function:
$V^\pi(s) := \sum_{a \in \mathcal{A}} \pi(a|s) \left( r(s, a) + \gamma \sum_{s' \in \mathcal{S}} P(s'|s, a) V^\pi(s') \right)$

Action value function:
$Q^\pi(s, a) := r(s, a) + \gamma \sum_{s' \in \mathcal{S}} P(s'|s, a) V^\pi(s')$
\end{mdframed}

Q means 'quality' by the way.
Given the initial state distribution $\mu(s)$, the goal of an agent is to find an optimal policy (there can be many though):
$\pi^* :=  \textrm{argmax}_\pi \sum_s \mu(s) V^\pi (s)$. In short, we denote this objective by $\mu V^\pi$. To define the optimality, we can also use the expected sum of discounted reward (i.e., $\sum_t^\infty \gamma^t r(S_t)$ where $S_t$ is a random variable that denote a state that an agent visits at time $t$) but this value-based version is simpler (and, both are the same).

You may also have learned some fancy value-based methods for getting $\pi^*$ purely from agent-environmental interaction, and they are clear and nice. So why do we need policy gradients? An advantage is being parameterized. This nature makes PG easier to extend with function approximation (e.g., deep learning!). A bad point of PG is its convergence to global optima is not ensured. It just finds a local maximum.

OK, let me introduce PG now. Policy is parameterized by a parameter $\theta$ and denoted as $\pi_\theta$. Then, policy gradient methods update $\theta$ using *some* gradient. That's all and there are many ways. The easiest and common one is to use the gradient of $\mu V^\pi$, which is exatly our objective, and do gradienct-ascent updates using $\nabla \mu V^\pi$. So how can we do it?

First, let me introduce the policy gradient theorem:

\begin{mdframed}
\textbf{Policy gradient theorem}
\begin{align}
\nabla \mu V^{\pi_\theta} &:= \mathbb{E} \sum_t r(s_t, a_t) \nabla \log \pi_\theta (a_t|s_t) \\
&:= \mathbb{E} \frac{1}{1 - \gamma} A^{\pi_\theta}(s, a) \nabla \log \pi_\theta (a|s)
\end{align}

\end{mdframed}

where an advantage function $A$ is $A^\pi(s, a) = Q^\pi(s, a) - V^\pi(s)$. The latter equation seems useful since it doesn't require the sum of rewards and easy to use with online updates. The proof is omitted and readers can refer to references.

OK, so we need to compute $\nabla \log \pi_\theta (a|s)$ for update, which depends on the paremetrization. For example, with deep neural network parametrization, the gradient can be so complex but some fancy auto differential framework like PyTorch can kindly compute the gradient.
Here, I use a common linear parametrization by softmax function: $\pi_\theta (a|s) = \frac{\exp(\theta_{s, a})}{\sum_{s'}\exp(\theta_{s', a})}$. Then, by some undergrad math, we observe that:

\begin{align}
\frac{\partial \log \pi(a|s)}{\partial \theta_{s', a'}} &=
  \begin{cases}
    0 & \text{if $s \neq s'$,} \\
    -\pi(a'|s) & \text{if $s=s'$ and $a\neq a'$,} \\
    1-\pi(a'|s) & \text{if $s=s'$ and $a = a'$.}
  \end{cases} \\
&= \indic{s = s'}(\indic{a = a'} - \pi(a'|s)).
\end{align}

Where $\indic{x}$ is the indicator function that takes $1$ when the condition is true and otherwise $0$.
Let's use this equation to have the partial gradient of $\mu V^\pi$!

\begin{align}
\frac{\partial \mu V^{\pi_\theta}}{\partial \theta_{s', a'}} &= \mathbb{E} \frac{1}{1 - \gamma} A^{\pi_\theta}(s, a) \frac{\partial \log \pi(a|s)}{\partial \theta_{s', a'}}  \\
&=
\frac{1}{1 - \gamma}
\mathbb{E}_s \mathbb{E}_{a\sim \pi_\theta(\cdot|s)} A^{\pi_\theta}(s, a)
\indic{s = s'}(\indic{a = a'} - \pi(a'|s')) \\
&=
\frac{1}{1 - \gamma}
\mathbb{E}_{s'} \mathbb{E}_{a\sim \pi_\theta(\cdot|s')} A^{\pi_\theta}(s', a)
(\indic{a = a'} - \pi(a'|s')) \\
&=
\frac{1}{1 - \gamma}
\mathbb{E}_{s'}
\left[
\underbrace{\mathbb{E}_{a\sim \pi_\theta(\cdot|s')} \left[\indic{a = a'} A^{\pi_\theta}(s', a)\right]}_{\mbox{\small = $\pi_\theta(a'|s')A^{\pi_\theta}(s', a')$}}
- \pi(a'|s') \underbrace{\mathbb{E}_{a\sim \pi_\theta(\cdot|s')} \left[A^{\pi_\theta}(s', a)\right]}_{\mbox{\small = $\mathbb{E}_{a\sim \pi_\theta(\cdot|s')} [Q(s', a)] - V(s') = 0$}}
\right] \\
&=
\frac{1}{1 - \gamma}
\mathbb{E}_{s'} \left[ \pi_\theta(a'|s')A^{\pi_\theta}(s', a') \right]
\end{align}

So now we got a handy equation that we can use for online policy update! The poor $\frac{1}{1 - \gamma}$ is often dropped, although the theory needes it.

To approximate $A^\pi$, considerably many quantities can be used. A method is called 'actor-critic' if it uses some value function approximation to estimate $A^{\pi}$. TD error is an example, although it's often a bit biased.

Here I list references:

+ Reinforcement Learning: Theory and Algorithms
  + Alekh Agarwal Nan Jiang Sham M. Kakade Wen Sun
  + https://rltheorybook.github.io/
+ Reinforcement Learning: An Introduction
  + Richard S. Sutton and Andrew G. Barto
  + http://www.incompleteideas.net/book/the-book.html
